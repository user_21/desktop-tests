export const tests = [
  {
    name: "test 1",
    visibility: true,
    maximumScore: 15,
    test: [
      {
        question: "Который час?",
        answers: [
          [false, "1 pm"],
          [false, "2 pm"],
          [false, "3 pm"],
          [true, "4 pm"]
        ]
      },
      {
        question: "Который час?",
        answers: [
          [false, "1 pm"],
          [false, "2 pm"],
          [false, "3 pm"],
          [true, "4 pm"]
        ]
      },
      {
        question: "Который час?",
        answers: [
          [false, "1 pm"],
          [false, "2 pm"],
          [false, "3 pm"],
          [true, "4 pm"]
        ]
      },
      {
        question: "Который час?",
        answers: [
          [false, "1 pm"],
          [false, "2 pm"],
          [false, "3 pm"],
          [true, "4 pm"]
        ]
      },
      {
        question: "Который час?",
        answers: [
          [false, "1 pm"],
          [false, "2 pm"],
          [false, "3 pm"],
          [true, "4 pm"]
        ]
      },
    ]
  },
  {
    name: "test 2",
    visibility: true,
    maximumScore: 15,
    test: [
      {
        question: "Который час?",
        answers: [
          [false, "1 pm"],
          [false, "2 pm"],
          [false, "3 pm"],
          [true, "4 pm"]
        ]
      },
      {
        question: "Который час?",
        answers: [
          [false, "1 pm"],
          [false, "2 pm"],
          [false, "3 pm"],
          [true, "4 pm"]
        ]
      },
      {
        question: "Который час?",
        answers: [
          [false, "1 pm"],
          [false, "2 pm"],
          [false, "3 pm"],
          [true, "4 pm"]
        ]
      },
      {
        question: "Который час?",
        answers: [
          [false, "1 pm"],
          [false, "2 pm"],
          [false, "3 pm"],
          [true, "4 pm"]
        ]
      },
      {
        question: "Который час?",
        answers: [
          [false, "1 pm"],
          [false, "2 pm"],
          [false, "3 pm"],
          [true, "4 pm"]
        ]
      },
    ]
  }, {
    name: "test 3",
    visibility: true,
    maximumScore: 12,
    test: [
      {
        question: "Который час?",
        answers: [
          [false, "1 pm"],
          [false, "2 pm"],
          [false, "3 pm"],
          [true, "4 pm"]
        ]
      },
      {
        question: "Который час?",
        answers: [
          [false, "1 pm"],
          [false, "2 pm"],
          [false, "3 pm"],
          [true, "4 pm"]
        ]
      },
      {
        question: "Который час?",
        answers: [
          [false, "1 pm"],
          [false, "2 pm"],
          [false, "3 pm"],
          [true, "4 pm"]
        ]
      },
      {
        question: "Который час?",
        answers: [
          [false, "1 pm"],
          [false, "2 pm"],
          [false, "3 pm"],
          [true, "4 pm"]
        ]
      },
      {
        question: "Который час?",
        answers: [
          [false, "1 pm"],
          [false, "2 pm"],
          [false, "3 pm"],
          [true, "4 pm"]
        ]
      },
    ]
  },
]