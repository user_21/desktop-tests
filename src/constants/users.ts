export const users = [
  {
    name: "user 1",
    tests: [
      {
        name: "test 1",
        maximumScore: 15,
        attemps: {
          attemp1: {
            time: ["24.10.2022 9:14", "400"],
            score: 15,
          },
          attemp2: {
            time: ["24.10.2022 9:14", "400"],
            score: 15,
          },
          attemp3: {
            time: ["24.10.2022 9:14", "400"],
            score: 10,
          },
        }
      },
      {
        name: "test 2",
        maximumScore: 15,
        attemps: {
          attemp1: {
            time: ["24.10.2022 9:14", "400"],
            score: 10,
          },
          attemp2: {
            time: ["24.10.2022 9:14", "400"],
            score: 10,
          },
        }
      }
    ]
  },
  {
    name: "user 2",
    tests: [
      {
        name: "test 1",
        maximumScore: 15,
        attemps: {
          attemp1: {
            time: ["24.10.2022 9:14", "400"],
            score: 15,
          },
          attemp2: {
            time: ["24.10.2022 9:14", "400"],
            score: 10,
          },
          attemp3: {
            time: ["24.10.2022 9:14", "400"],
            score: 10,
          },
        }
      },
      {
        name: "test 2",
        maximumScore: 15,
        attemps: {
          attemp1: {
            time: ["24.10.2022 9:14", "400"],
            score: 10,
          },
          attemp2: {
            time: ["24.10.2022 9:14", "400"],
            score: 10,
          },
        }
      },
      {
        name: "test 3",
        maximumScore: 12,
        attemps: {
          attemp1: {
            time: ["24.10.2022 9:14", "400"],
            score: 12,
          },
          attemp2: {
            time: ["24.10.2022 9:14", "400"],
            score: 10,
          },
        }
      }
    ]
  },
]