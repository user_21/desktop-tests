import './App.scss';
import { Route, Routes } from 'react-router-dom';
import LoginPage from './pages/loginPage/LoginPage';
import TestsPage from './pages/testsPage/TestsPage';
import UsersPage from './pages/usersPage/UsersPage';
import { AppRoutes, panelRoutes } from './core/config/routes.config';
import MainPage from './pages/mainPage/MainPage';

function App() {
  return (
    <Routes>
      <Route path={panelRoutes.getMainRoute()} element={<MainPage />}>
        <Route path={panelRoutes.getTestRoute()} element={<TestsPage />} />
        <Route path={panelRoutes.getUsersRoute()} element={<UsersPage />} />
      </Route>
      <Route path={AppRoutes.LOGIN_PAGE} element={<LoginPage />} />
    </Routes>
  );
}

export default App;
