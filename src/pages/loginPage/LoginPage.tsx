import "./loginPage.scss";
import { useState } from "react";
import { useNavigate } from "react-router-dom";
import { panelRoutes } from "../../core/config/routes.config";
import { admin } from "../../constants/admin";

interface LoginPageProps { }

const LoginPage = (props: LoginPageProps) => {

  let navigate = useNavigate();

  const [credentials, setCredentials] = useState({ login: "", password: "" });

  const login = () => {
    if (credentials.login == admin.login && credentials.password == admin.password) {
      localStorage.setItem("admin", "true");
    }
  }

  const submit = (e: any) => {
    e.preventDefault();
    login();
    navigate(panelRoutes.getTestRoute());
  }

  return (
    <div className="login-page-wrapper">

      <div className="login-page__form">

        <form onSubmit={submit} className="form__form-inner">

          <div className="form-group">
            <input
              className="form-group__input form-group__input--login"
              type="text"
              placeholder="login"
              onChange={(e) => setCredentials({ ...credentials, login: e.target.value })}
            />
          </div>

          <div className="form-group">
            <input
              className="form-group__input form-group__input--password"
              type="text"
              placeholder="password"
              onChange={(e) => setCredentials({ ...credentials, password: e.target.value })}
            />
          </div>

          <input
            className="submit-button"
            type="submit"
            value={"Login"}
          />

        </form>

      </div>
    </div>
  );
};

export default LoginPage;
