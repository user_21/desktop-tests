import "./testsPage.scss";
import { tests } from "../../constants/tests";
import { useState } from "react";
import Modal from "../../components/modal/Modal";

interface TestsPageProps { }

const TestsPage = (props: TestsPageProps) => {

  const [items, setItems] = useState(tests);
  const [data, setData] = useState({ name: "" });

  const [modalActive, setmodalActive] = useState(false);

  const [checked, setChecked] = useState<number>(0);

  const [questions, setQuestions] = useState<any>([]);

  const [numberQuestions, setNumberQuestions] = useState<number>(0);

  const [credentials, setCredentials] = useState({
    name: "",
    question: "",
    answer1: "",
    answer2: "",
    answer3: "",
    answer4: "",
  });

  const deleteTest = (name: any) => {
    let newItems = items
      .filter(item => item.name !== name);
    setItems(newItems);
  }

  const hideTest = (name: any) => {
    let newItems = items
      .map((item: any): any => {
        if (item.name == name) {
          let tmp = item;
          tmp.visibility = !tmp.visibility;
          return tmp;
        }
        return item;
      });

    setItems(newItems);
  }

  let test: any = {
    name: "",
    visibility: true,
    maximumScore: 0,
    test: []
  }

  let question: any = {
    question: "some",
    answers: []
  }

  const addTest = () => {
    test.name = credentials.name;
    if (question) {
      test.test.push(question);
      test.maximumScore = question.length
      let newItems = [...items];
      newItems.push(test);
      setItems(newItems);
    }

    alert(credentials.name + " successfully added!");

    setCredentials({
      name: "",
      question: "",
      answer1: "",
      answer2: "",
      answer3: "",
      answer4: "",
    });
  }

  const addQuestion = () => {
    question.question = credentials.question;
    question.answers.push([checked == 0 ? true : false, credentials.answer1]);
    question.answers.push([checked == 1 ? true : false, credentials.answer2]);
    question.answers.push([checked == 2 ? true : false, credentials.answer3]);
    question.answers.push([checked == 3 ? true : false, credentials.answer4]);
    questions.push(question)

    setCredentials({
      name: credentials.name,
      question: "",
      answer1: "",
      answer2: "",
      answer3: "",
      answer4: "",
    });

    setNumberQuestions(number => number + 1)
  }

  return (
    <div className="tests-page-wrapper">

      <div className="tests-page__top-panel">
        <div className="tests-page__top-container">
          <div className="tests-page__top-text-container">
            <p className="top-panel__text">Name:</p>
            <p className="top-panel__text">Visibility:</p>
          </div>
          <button
            className="top-panel__add-button"
            onClick={() => {
              setmodalActive(true);
              setNumberQuestions(0);
              setCredentials({
                name: "",
                question: "",
                answer1: "",
                answer2: "",
                answer3: "",
                answer4: "",
              })
            }}
          >
            Add
          </button>
        </div>
      </div>

      <div className="test-page__ul-wrapper">

        <ul className="test-page__ul">

          {items.map((item) => <li
            className="test-page__ul-item"
            key={item.name}
          >
            <div className="test-page__ul-item-container">

              <div className="ul-item__left-block-container">
                <div className="test-page__ul-item-name">
                  {item.name}
                </div>

                <span className={`ul-item__visibility-text${item.visibility === true ? "--hidden" : "--visible"}`}>
                  {item.visibility === true ? "hidden" : "visible"}
                </span>
              </div>

              <div className="ul-item__button-panel-wrapper">

                <button
                  className={`ul-item__button ul-item__button--hide`}
                  onClick={(e) => hideTest(item.name)}
                >
                  {item.visibility === true ? "show" : "hide"}
                </button>

                <button
                  className="ul-item__button ul-item__button--delete"
                  onClick={(e) => deleteTest(item.name)}
                >
                  Delete
                </button>

              </div>

            </div>
          </li>)}

        </ul>

      </div>

      <Modal
        active={modalActive}
        setActive={setmodalActive}
      >
        <span>
          *test should contain from 5 to 15 questions
        </span>

        <input
          className="modal__test-name modal__input modal-item"
          type="text"
          placeholder="test name"
          value={credentials.name}
          onChange={(e) => setCredentials({ ...credentials, name: e.target.value })}
        />

        <input
          className="modal__question modal__input modal-item"
          type="text"
          placeholder="question"
          value={credentials.question}
          onChange={(e) => setCredentials({ ...credentials, question: e.target.value })}
        />

        <div className="modal__answers-block">

          <div className="modal__answers-block--left">

            <div className="form-group modal-item">
              <input
                className="modal__input"
                type="text"
                placeholder="answer 1"
                value={credentials.answer1}
                onChange={(e) => setCredentials({ ...credentials, answer1: e.target.value })}
              />
              <input type="radio" checked={checked === 0} onChange={() => setChecked(0)} />

            </div>
            <div className="form-group modal-item">
              <input
                className="modal__input"
                type="text"
                placeholder="answer 2"
                value={credentials.answer2}
                onChange={(e) => setCredentials({ ...credentials, answer2: e.target.value })}
              />
              <input type="radio" checked={checked === 1} onChange={() => setChecked(1)} />

            </div>
            <div className="form-group modal-item">
              <input
                className="modal__input"
                type="text"
                placeholder="answer 3"
                value={credentials.answer3}
                onChange={(e) => setCredentials({ ...credentials, answer3: e.target.value })}
              />
              <input type="radio" checked={checked === 2} onChange={() => setChecked(2)} />

            </div>
            <div className="form-group modal-item">
              <input
                className="modal__input"
                type="text"
                placeholder="answer 4"
                value={credentials.answer4}
                onChange={(e) => setCredentials({ ...credentials, answer4: e.target.value })}
              />
              <input type="radio" checked={checked === 3} onChange={() => setChecked(3)} />
            </div>

          </div>

          <div className="answers-block__added-questions-text">
            added questions:
            <div className={`answers-block__added-questions-number${numberQuestions > 4 ? "--black" : "--red"}`}>
              {numberQuestions}
            </div>
          </div>

        </div>

        <button
          className="modal__button modal__button--question modal-item"
          onClick={addQuestion}
          disabled={
            (credentials.question && credentials.answer1 && credentials.answer2 && credentials.answer3 && credentials.answer4)
              ? false
              : true
          }
        >
          Add question
        </button>

        <button
          disabled={(numberQuestions < 5 || !credentials.name) ? true : false}
          className="modal__button modal__button--test modal-item"
          onClick={addTest}
        >
          Save test
        </button>
      </Modal>

    </div>
  );
};

export default TestsPage;
