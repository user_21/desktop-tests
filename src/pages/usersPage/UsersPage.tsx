import "./usersPage.scss";
import { tests } from "../../constants/tests";
import { users } from "../../constants/users";
import { useState } from "react";
import Dropdown from "../../components/dropdown/Dropdown";
import TestStatistics from "../../components/testStatistics/TestStatistics";
import Modal from "../../components/modal/Modal";

interface UsersPageProps { }

const UsersPage = (props: UsersPageProps) => {

  const [items, setItems] = useState(users);
  const [data, setData] = useState({ name: "" });
  const [currentTest, setCurrentTest] = useState();
  const [modalActive, setmodalActive] = useState(false);

  const deleteTest = (name: any) => {
    let newItems = items
      .filter(item => item.name !== name);
    setItems(newItems);
  }

  const hideTest = (name: any) => {
    let newItems = items
      .map((item: any): any => {
        if (item.name == name) {
          let tmp = item;
          tmp.visibility = !tmp.visibility;
          return tmp;
        }
        return item;
      });

    setItems(newItems);
  }

  const testHandler = (value: any) => {
    setCurrentTest(value);
    setmodalActive(true);
  }

  return (
    <div className="users-page-wrapper">

      <div className="users-page__top-panel">

        <div className="users-page__top-container">
          <p className="top-panel__text">Name:</p>
          <p className="top-panel__text--action">Action:</p>
        </div>

      </div>

      <div className="user-page__ul-wrapper">

        <ul className="user-page__ul">

          {items.map((item) => <li
            className="user-page__ul-item"
            key={item.name}
          >
            <div className="test-page__ul-item-container">
              {item.name}
              <Dropdown
                values={item.tests}
                onChange={(value) => {
                  testHandler(value);
                }}
              />
            </div>
          </li>)}

        </ul>
      </div>

      <Modal
        active={modalActive}
        setActive={setmodalActive}
      >
        <TestStatistics value={currentTest}/>
      </Modal>
    </div>
  );
};

export default UsersPage;
