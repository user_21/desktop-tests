import "./mainPage.scss";
import { NavLink, Outlet } from "react-router-dom";
import { AppRoutes, panelRoutes } from '../../core/config/routes.config';
import { useNavigate } from "react-router-dom";
import { useEffect, useState } from "react";

interface MainPageProps { }

const MainPage = (props: MainPageProps) => {

  let navigate = useNavigate();

  const [selectTests, setSelectTests] = useState("-selected");
  const [selectUsers, setSelectUsers] = useState("");

  const [credentials, setCredentials] = useState({ login: "", password: "" });


  let login = localStorage.getItem("admin") == "true";

  useEffect(() => {
    if (!login) {
      navigate(AppRoutes.LOGIN_PAGE);
    }
  }, [])

  const logout = () => {
    localStorage.clear();
    navigate(AppRoutes.LOGIN_PAGE);
  }

  const togglerTests = () => {
    setSelectTests("-selected");
    setSelectUsers("");
  }

  const togglerUsers = () => {
    setSelectTests("");
    setSelectUsers("-selected");
  }

  return (
    <div className="main-page-wrapper">

      <div className="main-page__header">
        <div className="main-page__header-container">
          <div className="header__navbar-wrapper">
            <a
              className="navbar__item navbar__item--logout-button"
              onClick={logout}
            >
              Logout
            </a>
            <div className="navbar__item-wrapper">
              <NavLink
                className={`navbar__item navbar__item--navigation navbar__item--navigation${selectTests}`}
                onClick={togglerTests}
                to={panelRoutes.getTestRoute()}
              >
                Tests
              </NavLink>
            </div>
            <div className="navbar__item-wrapper">
              <NavLink
                className={`navbar__item navbar__item--navigation navbar__item--navigation${selectUsers}`}
                onClick={togglerUsers}
                to={panelRoutes.getUsersRoute()}
              >
                Users
              </NavLink>
            </div>
          </div>
        </div>
      </div>

      <div className="main-page__content">
        <Outlet />
      </div>

    </div>
  );
};

export default MainPage;
