import "./testStatistics.scss";
import { tests } from "../../constants/tests";
import { users } from "../../constants/users";
import { useState } from "react";
import { Value } from "sass";
import Modal from "../modal/Modal";

interface TestStatisticsrops {
  value: any;
}

const TestStatistics = (props: TestStatisticsrops) => {

  let maximumScore: any = props.value ? props.value.maximumScore : 0;
  let numberAttemps = props.value ? Object.keys(props.value.attemps).length : 0;

  let goodAttemps = 0;
  let attemps = props.value ? props.value.attemps : null;
  let sumScrore = 0;

  for (let attemp in attemps) {
    sumScrore += attemps[attemp].score;
    if (attemps[attemp].score == props.value.maximumScore) {
      goodAttemps++;
    }
  }

  let average = (sumScrore / numberAttemps).toFixed(0);

  return (
    <div className="statistics-page-wrapper">
      <div className="statistics__head-text">
        {props.value ? props.value.name : null} statistics:
      </div>
      <div>
        <div>number of attempts: {numberAttemps}</div>
        <div>number of successfully completed: {goodAttemps} ({(goodAttemps * 100 / numberAttemps).toFixed(2)} %) </div>
        <div>average score: {average}</div>
      </div>
    </div>
  );
};

export default TestStatistics;
