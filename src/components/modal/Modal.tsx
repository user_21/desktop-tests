import "./modal.scss";

interface ModalProps {
  active: any,
  setActive: any,
  children: any
}

const Modal = (props: ModalProps) => {
  return (
    <div
      className={props.active ? "modal active" : "modal"}
      onClick={() => {
        props.setActive(false)
      }}
    >
      <div
        className={props.active ? "modal__content active" : "modal__content"}
        onClick={e => e.stopPropagation()}
      >
        <div 
        className="close-button"
        onClick={() => props.setActive(false)}
        >
        </div>
        {props.children}
      </div>
    </div>
  )
}

export default Modal;