import { useState, useRef, useEffect } from "react";
import "./dropdown.scss";

interface DropdownProps {
  values: any;
  onChange?: (value: string) => void;
}

const Dropdown = (props: DropdownProps) => {
  const [dropdownState, setDropdownState] = useState({ open: false });

  const handleDropdownClick = () =>
    setDropdownState({ open: !dropdownState.open });

  const [isOpen, setIsOpen] = useState<boolean>(false);
  const [selectedOption, setSelectedOption] = useState();

  const toggling = () => setIsOpen(!isOpen);

  const onOptionClicked = (value: any) => () => {
    setSelectedOption(value);
    setIsOpen(false);
    props.onChange!(value);
  };

  return (
    <div className="dropdown">

      <button
        className="dropdown__button"
        type="button"
        onClick={handleDropdownClick}
      >
        {dropdownState.open ? "hide tests" : "show tests"}
      </button>

      {dropdownState.open && (

        <ul className="dropdown__ul">
          {props.values.map((option: any, index: any) => {
            return (
              <li
                className="dropdown__ul-item"
                key={index}
                onClick={onOptionClicked(option)}
              >
                {option.name}
              </li>
            );
          })}
        </ul>

      )}
    </div>
  );
}

export default Dropdown;