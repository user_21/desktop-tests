export enum AppRoutes {
  MAIN_PAGE = "/",
  LOGIN_PAGE = "/login",
}

export enum AppPanelMatchRoutes {
  TESTS_PAGE = "tests",
  USERS_PAGE = "users",
}

export const panelRoutes = {
  getMainRoute: () => `${AppRoutes.MAIN_PAGE}`,
  getTestRoute: () => `${AppRoutes.MAIN_PAGE}${AppPanelMatchRoutes.TESTS_PAGE}`,
  getUsersRoute: () => `${AppRoutes.MAIN_PAGE}${AppPanelMatchRoutes.USERS_PAGE}`,
};