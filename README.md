# A web application that allows you to administer the testing system

## Demo 
(https://drive.google.com/file/d/1FVcY5LJfRimDGgfc5CbDLguLx03dxvCV/view?usp=sharing)

### Task list

- Implement user authorization.
- Create login form.
- Сreate tests page.
- Create users page.
- Create modal window.
- Create additional components.
- Test the system.

###  Technology Stack

- React
- Typescript
- SCSS

### Install and run

- `git clone https://gitlab.com/user_21/desktop-tests`
- `npm i`
- `npm start`

### Login and password to test:
Login: Administrator
Password: 12345

#### Good luck!
